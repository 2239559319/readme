# 使用说明

使用电脑浏览器操作(不要用ie浏览器)

## 步骤

1. 用电脑浏览器登录抢hpv的网站

2. 复制下面代码到粘贴板
```javascript
const rootVm = document.querySelector('#app').__vue__;
const http = rootVm.$http;

async function getBookList(hospitalId) {
  const user = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const res = await http.get(`nc-person/book-date/list/${user.id}/${hospitalId}`);
  return res.data;
}

async function submit({
  date,
  time,
  hospitalId
}) {
  const user = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const postData = {
    personId: user.id,
    bookingFlag: 2,
    bookingTime: `${date} ${time}`,
    hospitalId,
  };
  const res = await http.post('nc-person/add/booking', postData);
  return res;
}

function action(hospitalId) {
  let count = 1;
  const timer = setInterval(async () => {
    const dateList = await getBookList(hospitalId);
    let isSelect = false;
    let date;
    let time;
    for (const resdate of dateList) {
      const { bookingDate, datePeriod } = resdate;
      for (const period of datePeriod) {
        const { bookingTime, surplusCounts, openFlag, timeFlag } = period;
        if (surplusCounts != 0 && openFlag != 0 && timeFlag != 0) {
          isSelect = true;
          date = bookingDate;
          time = bookingTime;
          break;
        }
      }
      if (isSelect) {
        break;
      }
    }
    if (isSelect && date && time) {
      const res = await submit({date, time, hospitalId});
      if (res.code == 200) {
        console.log('成功');
        console.log(`已选${date} ${time}`);
        clearInterval(timer);
      }
    }
    console.log(`已选${count}次`);
    count++;
  }, 500);
  return timer;
}

(() => {
  action(3);
})();

```

3. 在网站按f12打开调试，然后找到console(控制台)

就是下图这个

![](./dist/console.png)

4. 把复制好的代码粘贴(ctrl v)上去

![](./dist/code.png)

5. 在正式开始抢之前1分钟在console的代码下面按回车，此时开始抢并且有对应的输出。成功的话会有成功在控制台输出

